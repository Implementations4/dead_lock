#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <string>

std::mutex mutex1, mutex2;
bool deadlock = true;

void threadFunction1()
{
     std::lock_guard< std::mutex > lock1( mutex1 );
     std::this_thread::sleep_for( std::chrono::milliseconds( 1000 ) );
     if( deadlock )
     {
          std::lock_guard< std::mutex > lock2( mutex2 );
     }
}

void threadFunction2()
{
     std::lock_guard< std::mutex > lock2( mutex2 );
     std::this_thread::sleep_for( std::chrono::milliseconds( 1000 ) );
     if( deadlock )
     {
          std::lock_guard< std::mutex > lock1( mutex1 );
     }
}

int main( int argc, char* argv[] )
{

     if( argc > 1 )
     {
          std::string opt = argv[ 1 ];
          if( opt == "false" )
          {
               deadlock = false;
          }
          else if( opt != "true" )
          {
               std::cout << "Error: Invalid argument '" << opt << "'. ";
               std::cout << "Usage: "
                         << " [true|false]" << std::endl;
               return 1;
          }
     }

     std::thread t1( threadFunction1 );
     std::thread t2( threadFunction2 );

     t1.join();
     t2.join();

     std::this_thread::sleep_for( std::chrono::milliseconds( 5000 ) );

     return 0;
}
