#!/bin/bash

g++ -g -o dead_lock main.cpp -pthread

if pgrep dead_lock >/dev/null; then
    pkill -9 dead_lock >/dev/null 2>&1
fi

./dead_lock $1 &

pid=$(pgrep dead_lock)

sleep 3

if [ -z "$pid" ]; then
    echo "Error: Process dead_lock not found."
    exit 1
fi

output=$(sudo gdb -q -p $pid -ex "info threads" -ex "detach" -ex quit 2>/dev/null | grep lock_wait)

if [ -n "$output" ]; then
    echo "Test Failed: Deadlock detected"
    pkill -9 dead_lock > /dev/null 2>&1
else
    echo "Test Passed: No deadlock detected"
    wait $pid
fi

